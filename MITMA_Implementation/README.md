# Readme
- run BobServer
- run MallorysServer
- run AlicesClient with parameters name and message, e.g. "python3 AlicesClient alice "hey bob whats up""
- atm AlicesClient will connect to MallorysServer which will connect to bobs server which will connect to BobServer and return BobServer's response. Doing this, MallorysServer displays the info sent by AlicesClient on its standard output.

# Todo
- atm AlicesClient connects to MallorysServer by default. To change this host files need to be modified and BobServer (or MallorysServer) needs to be run on a different machine(or at least a different ip) because hosts file only allows ip manipulation, not port manipulation. At the moment Mallory and Bob run on different ports, this needs to be changed too.
