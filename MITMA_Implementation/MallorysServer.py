from http.server import HTTPServer, BaseHTTPRequestHandler
import ssl
import AlicesClient
from urllib.parse import urlparse, parse_qs

mallorys_address = 'FILLMEWITHIP'

class MallorysHTTPRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        #extract Alice name and message from the querystr
        parsed_path = urlparse(self.path)
        query_params = parse_qs(parsed_path.query)
        name = ""
        message = ""
        if "name" in query_params:
            name = "".join(query_params["name"])
        if "message" in query_params:
            message = "".join(query_params["message"])
        print("Alice sent:\n\tname:\t"+name+"\n\tmessage:\t"+message)


        #send the request to bob
        response = AlicesClient.calls("localhost:4442",name,message)

        self.send_response(response.status)
        for header in response.getheaders():
            self.send_header(header[0],header[1])
            #print(header_key+" "+header_value+"\n")
        self.end_headers()

        response_body = response.read()
        print(response_body)
        self.wfile.write(response_body)

# man könnte hier auch do_POST implementieren


httpd = HTTPServer((mallorys_address, 4442),MallorysHTTPRequestHandler)


httpd.socket = ssl.wrap_socket (httpd.socket,
        keyfile="./mallorys-key.pem", # private Key, der zu dem public Key passt, der im nachfolgenden Zertifikat enthalten ist
        certfile='./mallorys-cert.pem', server_side=True) # mit openSSL erzeugtes selbstsigniertes Zertifikat

httpd.serve_forever()
