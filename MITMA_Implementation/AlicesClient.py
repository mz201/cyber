import http.client
from urllib.parse import urlencode
import argparse
import ssl
import CertificateHasher

bobs_address = "bob.server:4442"
bobs_address_without_port = "bob.server"
bobsKey = "YjQwZWJhOTkyNGE0MTk1YTQ3ODgwNTliYjM4MmNjODIyZjU0MWYyZTVhOGQ3OGZkM2M3ZmI5MzMxZTgwOWNmYQ=="


def calls(adress,name, message):
    connection = http.client.HTTPSConnection(adress,context=ssl._create_unverified_context())#dont verify ssl certs
    query_string = urlencode({"name" : name,"message" : message},True)
    print("querystr:"+query_string)
    connection.request("GET", "/?" + query_string)
    response = connection.getresponse()
    r = response
    connection.close()
    return r


def main():
    parser = argparse.ArgumentParser(description='Send a get request to a predefined host.')
    parser.add_argument('name', metavar='name', type=str,
                       help='The content of the name value passed via the GET request')
    parser.add_argument('message', metavar='message', type=str,
                       help='The content of the message value passed via the GET request')
    args = parser.parse_args()




    # vielleicht ist das an einer anderen stelle sinnvoller
    certificate = ssl.get_server_certificate((bobs_address_without_port, 4442))
    hashKey = CertificateHasher.hash_certificate(certificate)
    print("Hashed Certificate Key is " + hashKey + "\n")
    if hashKey != bobsKey:
        print("Invalid certificate, expected " + bobsKey + "\n\n\n")
        #return # wenn alice sich trotzdem verbinden soll das return-statement hier kommentieren

    response = calls(bobs_address,args.name,args.message)


    print("{} {}\n".format(response.status, response.reason))
    print(str(response.read(), "utf-8"))


if __name__ == "__main__":
    # execute only if run as a script
    main()
