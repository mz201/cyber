from Crypto.Util.asn1 import DerSequence
from binascii import a2b_base64
import hashlib
import base64

def hash_certificate(certificate):
    derCertificate = convert_pem_to_dem_certificate(certificate)
    publicKey = extract_public_key_info(derCertificate)
    encodedKey = hash_public_key(publicKey)
    return encodedKey


def convert_pem_to_dem_certificate(certificate):
    lines = certificate.replace(" ", '').split()
    der = a2b_base64(''.join(lines[1:-1]))
    return der


def extract_public_key_info(demCertificate):
    certificate = DerSequence()
    certificate.decode(demCertificate)
    tbsCertificate = DerSequence()
    tbsCertificate.decode(certificate[0])
    publicKeyInfo = tbsCertificate[6]
    return publicKeyInfo


def hash_public_key(publicKey):
    hash_object = hashlib.sha256(publicKey)
    hex_dig = hash_object.hexdigest()
    encodedBytes = base64.b64encode(hex_dig.encode("utf-8"))
    encodedString = str(encodedBytes, "utf-8")
    return encodedString
