# https web server based on andreas work
# handles get requests with parameters 'name' and 'message' and displays their content in simple document

from http.server import HTTPServer, BaseHTTPRequestHandler
import ssl
from urllib.parse import urlparse, parse_qs


bobs_address = 'FILLMEWITHIP'


class BobsHTTPRequestHandler(BaseHTTPRequestHandler):
    messages = [] #list of tuples (name,message)
    def do_GET(self):
        self.send_response(200)
        self.end_headers()
        parsed_path = urlparse(self.path)
        query_params = parse_qs(parsed_path.query)
        name = None
        message = None
        if "name" in query_params:
            name = "".join(query_params["name"])
        if "message" in query_params:
            message = "".join(query_params["message"])
        if not name is None and not message is None: #add message to the board
            self.messages.append((name,message))
        for posted_message in self.messages:
            self.wfile.write((""+posted_message[0]+" says:\n"+posted_message[1]+"\n").encode())
        #self.wfile.write((self.command+" "+self.path).encode())

# man könnte hier auch do_POST implementieren


httpd = HTTPServer((bobs_address, 4442),BobsHTTPRequestHandler)


httpd.socket = ssl.wrap_socket (httpd.socket,
        keyfile="./bobs-key.pem", # private Key, der zu dem public Key passt, der im nachfolgenden Zertifikat enthalten ist
        certfile='./bobs-cert.pem', server_side=True) # mit openSSL erzeugtes selbstsigniertes Zertifikat


httpd.serve_forever()
