# What do we do?
*  Subtasks:

     * Research MITM Attacks and draw a diagram
     * Ways to intercept the communication (e.g public hotspot, fake hotspot, ...)
     
     * Implementation of a MITM-attack (e.g client application which intercepts communications)
     
     * Implementation of a webserver and a client (e.g HTTP(S) Server in Python) ::WEG
     
     * Ways to defent against MITM attacks (certificate pinning, ...)
     * Implementation of the defence against MITM attacks
     
*  Presentation

# Who does?
*  Conrad: Research how to intercept on network level + Implementation of Client&Server
*  Michael: Research ways to defend against MITM attacks + Implementation of MITM Defence (unless we find a 4th Point for Implementation)
*  Niklas: Research ways to defend against MITM attacks + Implementation of MITM Defence (unless we find a 4th Point for Implementation)
*  Andreas: Implementation of a MITM attack

# Time planning
*  April 24: First report and research
*  First: Client&Server Implementation
*  Second: MITM Attack Implementation
*  Third: MITM Defence Implementation
 
# Für jedes Subthema
* Welche Methoden? 
* Welche Probleme?